import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#673ab7",
    },
    secondary: {
      main: "#2979ff",
    },
    background: {
      default: `transparent`,
    },
  },
  typography: {
    fontFamily: "'Ubuntu', sans-serif;",
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        html: {
          background: `linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);`,
        },
      },
    },
  },
});

export default theme;
