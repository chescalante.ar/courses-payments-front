import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Layout from "../Layout";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import TextField from "@material-ui/core/TextField";
import { useForm, Controller } from "react-hook-form";
import Button from "@material-ui/core/Button";

import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

import axios from "axios";
import environment from "../../environment";

const AddPreference = () => {
  const classes = useStyles();

  const {
    register,
    handleSubmit,
    errors,
    control,
    reset,
    formState,
  } = useForm();

  const onSubmit = async (data) => {
    await axios.post(`${environment.apiUrl}/payments/add-preference`, data);
  };

  const { isSubmitSuccessful, isSubmitting } = formState;

  return (
    <Layout title={"Payment preference"} backUrl={"/"}>
      <Card className={classes.root}>
        <CardHeader
          title={
            isSubmitSuccessful
              ? "Payment preference created!!"
              : "Create a new payment preference"
          }
        />
        <CardContent>
          {isSubmitSuccessful && (
            <Button
              fullWidth
              size="large"
              variant="contained"
              color="secondary"
              onClick={() => reset()}
            >
              Create another payment preference
            </Button>
          )}
          <form
            onSubmit={handleSubmit(onSubmit)}
            style={{
              display: isSubmitSuccessful ? "none" : "flex",
              flexDirection: "column",
              gap: "10px",
            }}
          >
            <TextField
              inputRef={register({
                required: "Your name is required",
              })}
              name="name"
              fullWidth
              label="Name"
              variant="filled"
              error={!!errors.name}
              helperText={!!errors.name ? errors.name.message : ""}
            />
            <TextField
              inputRef={register({
                required: "Your email is required",
                pattern: {
                  value: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
                  message: "Invalid email address",
                },
              })}
              name="email"
              fullWidth
              label="Email"
              variant="filled"
              error={!!errors.email}
              helperText={!!errors.email ? errors.email.message : ""}
            />
            <TextField
              inputRef={register}
              name="career"
              fullWidth
              label="Career"
              variant="filled"
              error={!!errors.career}
              helperText={!!errors.career ? errors.career.message : ""}
            />
            <TextField
              inputRef={register}
              name="dateOfBirth"
              fullWidth
              label="Birthday"
              type="date"
              variant="filled"
              InputLabelProps={{
                shrink: true,
              }}
              error={!!errors.dateOfBirth}
              helperText={
                !!errors.dateOfBirth ? errors.dateOfBirth.message : ""
              }
            />
            <TextField
              inputRef={register({
                required: "Your phone number is required",
                pattern: {
                  value: /^\d+$/,
                  message: "Invalid phone number",
                },
              })}
              name="phoneNumber"
              fullWidth
              label="Phone number"
              type="tel"
              variant="filled"
              error={!!errors.phoneNumber}
              helperText={
                !!errors.phoneNumber ? errors.phoneNumber.message : ""
              }
            />
            <TextField
              inputRef={register({
                required: "The country is required",
              })}
              name="country"
              fullWidth
              label="Country"
              variant="filled"
              error={!!errors.country}
              helperText={!!errors.country ? errors.country.message : ""}
            />
            <TextField
              inputRef={register({
                required: "The city is required",
              })}
              name="city"
              fullWidth
              label="City"
              variant="filled"
              error={!!errors.city}
              helperText={!!errors.city ? errors.city.message : ""}
            />
            <ReactHookFormSelect
              name="paymentOption"
              label="Payment option"
              control={control}
              variant="filled"
              defaultValue={""}
              error={!!errors.paymentOption}
              helperText={
                !!errors.paymentOption ? errors.paymentOption.message : ""
              }
            >
              <MenuItem value={"CreditCard"}>Credit Card</MenuItem>
              <MenuItem value={"DebitCard"}>Debit Card</MenuItem>
              <MenuItem value={"Cash"}>Cash</MenuItem>
              <MenuItem value={"Transfer"}>Transfer</MenuItem>
            </ReactHookFormSelect>
            <ReactHookFormSelect
              name="paymentInstallments"
              label="Payment installments"
              control={control}
              variant="filled"
              defaultValue={""}
              error={!!errors.paymentInstallments}
              helperText={
                !!errors.paymentInstallments
                  ? errors.paymentInstallments.message
                  : ""
              }
            >
              <MenuItem value={1}>1</MenuItem>
              <MenuItem value={3}>3</MenuItem>
              <MenuItem value={6}>6</MenuItem>
            </ReactHookFormSelect>
            <Button
              type="submit"
              fullWidth
              size="large"
              variant="contained"
              color="secondary"
              disabled={isSubmitting}
            >
              Save
            </Button>
          </form>
        </CardContent>
      </Card>
    </Layout>
  );
};

export default AddPreference;

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
});

const ReactHookFormSelect = ({
  name,
  label,
  control,
  defaultValue,
  helperText,
  children,
  ...props
}) => {
  const labelId = `${name}-label`;
  return (
    <FormControl {...props}>
      <InputLabel id={labelId}>{label}</InputLabel>
      <Controller
        as={
          <Select labelId={labelId} label={label}>
            {children}
          </Select>
        }
        rules={{
          required: true,
        }}
        name={name}
        control={control}
        defaultValue={defaultValue}
      />
      <FormHelperText>{helperText}</FormHelperText>
    </FormControl>
  );
};
