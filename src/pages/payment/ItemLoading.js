import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
});

const ItemLoading = () => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          <Skeleton animation="wave" width="80%" />
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          <Skeleton animation="wave" width="60%" />
        </Typography>
      </CardContent>
    </Card>
  );
};

export default ItemLoading;
