import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Layout from "../Layout";
import ItemLoading from "./ItemLoading";

import pluralize from "pluralize";
import environment from "../../environment";
import axios from "axios";
import Button from "@material-ui/core/Button";

import { Link } from "react-router-dom";

const Preferences = () => {
  const [preferences, setPreferences] = useState(null);

  useEffect(() => {
    const apiCall = async () => {
      const apiUrl = `${environment.apiUrl}/payments`;

      const { data } = await axios.get(apiUrl);

      setPreferences(data);
    };

    apiCall();
  }, []);

  return (
    <Layout title={"Payment preferences"}>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          gap: 20,
        }}
      >
        <div style={{ alignSelf: "flex-end" }}>
          <Button
            variant="contained"
            color="secondary"
            component={Link}
            to="/create"
          >
            Add preference
          </Button>
        </div>
        {preferences && preferences.length === 0 && (
          <div>No payment preferences yet.</div>
        )}
        {preferences && preferences.map((value) => <Item preference={value} />)}
        {!preferences && (
          <>
            <ItemLoading />
            <ItemLoading />
            <ItemLoading />
          </>
        )}
      </div>
    </Layout>
  );
};

export default Preferences;

const Item = ({
  preference: { name, email, paymentOption, paymentInstallments },
}) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography gutterBottom variant="h6" component="span">
          {`${name} (${email})`}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {`${paymentInstallments} ${pluralize(
            "installment",
            paymentInstallments
          )} - ${paymentOption}`}
        </Typography>
      </CardContent>
    </Card>
  );
};

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
});
