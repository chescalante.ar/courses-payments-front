This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Setup

```

1. Rename .env.sample to .env

2. Put the api url into your .env file

2. npm install

```


## Test

```
npm run test
```

## Development

```
npm start
```
